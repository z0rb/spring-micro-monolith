package com.devfiant.spring.boot.micromonolith.monolith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.devfiant.spring.boot.micromonolith.component_a",
"com.devfiant.spring.boot.micromonolith.component_b" })
public class MonolithApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonolithApplication.class, args);
	}
	
}
