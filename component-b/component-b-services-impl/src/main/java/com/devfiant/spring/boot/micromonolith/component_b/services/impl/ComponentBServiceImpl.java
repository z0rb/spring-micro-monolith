package com.devfiant.spring.boot.micromonolith.component_b.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.devfiant.spring.boot.micromonolith.component_b.services.ComponentBService;
import com.devfiant.spring.boot.micromonolith.services.Implementation;

@Service
public class ComponentBServiceImpl implements ComponentBService{

	private Logger logger = LoggerFactory.getLogger(ComponentBServiceImpl.class);
	
	@Override
	public Implementation getImplementation() {
		
		logger.info("Component B business logic implementation invoked!");
		
		return new Implementation(this.getClass().getName());
	}

}
