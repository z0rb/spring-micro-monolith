package com.devfiant.spring.boot.micromonolith.component_b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.devfiant.spring.boot.micromonolith.component_a",
		"com.devfiant.spring.boot.micromonolith.component_b" })
public class ComponentBApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComponentBApplication.class, args);
	}
}
