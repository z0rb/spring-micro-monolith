package com.devfiant.spring.boot.micromonolith.component_b.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devfiant.spring.boot.micromonolith.component_a.services.ComponentAService;
import com.devfiant.spring.boot.micromonolith.component_b.services.ComponentBService;
import com.devfiant.spring.boot.micromonolith.services.Implementation;


@RestController
@RequestMapping("component-b")
public class ComponentBApi {
	
	Logger logger = LoggerFactory.getLogger(ComponentBApi.class);
	
	@Autowired
	private ComponentBService componentBService;
	
	@Autowired
	ComponentAService componentAService;

	@RequestMapping("main")
	public Implementation callOwnService() {
		
		logger.info("/component-b/main request received ...");
		
		return componentBService.getImplementation();
	}
	
	@RequestMapping("component-a-service")
	public Implementation callComponentBService() {
		return componentAService.getImplementation();
	}
}
