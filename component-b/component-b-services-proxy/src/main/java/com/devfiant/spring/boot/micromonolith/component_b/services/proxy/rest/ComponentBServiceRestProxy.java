package com.devfiant.spring.boot.micromonolith.component_b.services.proxy.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.devfiant.spring.boot.micromonolith.component_b.services.ComponentBService;
import com.devfiant.spring.boot.micromonolith.services.Implementation;

@Service
public class ComponentBServiceRestProxy implements ComponentBService {
	
	private Logger logger = LoggerFactory.getLogger(ComponentBServiceRestProxy.class);

	@Override
	public Implementation getImplementation() {
		
		logger.info("Calling component b using REST client ...");
		
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject("http://localhost:8090/component-b/main", Implementation.class);
	}

}
