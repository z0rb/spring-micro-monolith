package com.devfiant.spring.boot.micromonolith.component_b.services;

import com.devfiant.spring.boot.micromonolith.services.Implementation;

public interface ComponentBService {
	
	Implementation getImplementation();

}
