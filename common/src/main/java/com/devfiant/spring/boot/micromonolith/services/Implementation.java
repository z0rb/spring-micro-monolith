package com.devfiant.spring.boot.micromonolith.services;

public class Implementation {
	
	private String implementation;
	
	public Implementation() {}
	
	public Implementation(String implementation) {
		this.implementation = implementation;
	}
	
	public String getImplementation() {
		return implementation;
	}
	
	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}

}
