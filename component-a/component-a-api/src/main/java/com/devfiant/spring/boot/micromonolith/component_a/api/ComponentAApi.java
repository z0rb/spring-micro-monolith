package com.devfiant.spring.boot.micromonolith.component_a.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devfiant.spring.boot.micromonolith.component_a.services.ComponentAService;
import com.devfiant.spring.boot.micromonolith.component_b.services.ComponentBService;
import com.devfiant.spring.boot.micromonolith.services.Implementation;


@RestController
@RequestMapping("component-a")
public class ComponentAApi {
	
	Logger logger = LoggerFactory.getLogger(ComponentAApi.class);
	
	@Autowired
	private ComponentAService componentAService;
	
	@Autowired
	private ComponentBService componentBService;

	@RequestMapping("main")
	public Implementation callOwnService() {
		
		logger.info("/component-a/main request received ...");
		
		return componentAService.getImplementation();
	}
	
	@RequestMapping("component-b-service")
	public Implementation callComponentBService() {
		return componentBService.getImplementation();
	}
}
