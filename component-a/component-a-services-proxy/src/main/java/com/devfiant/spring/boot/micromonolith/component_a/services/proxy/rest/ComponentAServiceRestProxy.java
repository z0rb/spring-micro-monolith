package com.devfiant.spring.boot.micromonolith.component_a.services.proxy.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.devfiant.spring.boot.micromonolith.component_a.services.ComponentAService;
import com.devfiant.spring.boot.micromonolith.services.Implementation;

@Service
public class ComponentAServiceRestProxy implements ComponentAService {
	
	private Logger logger = LoggerFactory.getLogger(ComponentAServiceRestProxy.class);

	@Override
	public Implementation getImplementation() {
		
		logger.info("Calling component a using REST client ...");
		
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject("http://localhost:8080/component-a/main", Implementation.class);
	}

}
