package com.devfiant.spring.boot.micromonolith.component_a.services;

import com.devfiant.spring.boot.micromonolith.services.Implementation;

public interface ComponentAService {
	
	Implementation getImplementation();

}
