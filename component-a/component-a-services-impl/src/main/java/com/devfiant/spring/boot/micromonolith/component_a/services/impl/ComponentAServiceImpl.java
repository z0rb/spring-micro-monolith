package com.devfiant.spring.boot.micromonolith.component_a.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.devfiant.spring.boot.micromonolith.component_a.services.ComponentAService;
import com.devfiant.spring.boot.micromonolith.services.Implementation;

@Service
public class ComponentAServiceImpl implements ComponentAService{

	private Logger logger = LoggerFactory.getLogger(ComponentAServiceImpl.class);
	
	@Override
	public Implementation getImplementation() {
		
		logger.info("Component A business logic implementation invoked!");
		
		return new Implementation(this.getClass().getName());
	}

}
