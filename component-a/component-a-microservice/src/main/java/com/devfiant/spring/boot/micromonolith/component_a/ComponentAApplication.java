package com.devfiant.spring.boot.micromonolith.component_a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.devfiant.spring.boot.micromonolith.component_a", 
		"com.devfiant.spring.boot.micromonolith.component_b"})
public class ComponentAApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComponentAApplication.class, args);
	}
}
